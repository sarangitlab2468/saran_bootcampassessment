package pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ServiceConsolePage extends PreAndPost{
	public ServiceConsolePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
public ServiceConsolePage clickDropDownMenu() {
		
		try {
			//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
			click(locateElement("xpath","//button[@title=\"Show Navigation Menu\"]"));
			Thread.sleep(1000);
			System.out.println("FileDropdown menu Button clicked");


		} catch (Exception e) {
			System.out.println("FileDropdown Button not Clicked");
			e.printStackTrace();
		}
	return this; 	
}
	
	public ServiceConsolePage clickFilesFromDropdown() {
		
		try {
			//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
			click(locateElement("xpath","//ul/li//span[text()='Files']"));
			System.out.println("File Button clicked");


		} catch (Exception e) {
			System.out.println("File Button not Clicked");
			e.printStackTrace();
		}
	return this; 

	}
	
	
public ServiceConsolePage clickUploadFiles() {
		
		try {
			//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
			click(locateElement("xpath","//div[@title=\"Upload Files\"]"));
			Thread.sleep(1000);
			System.out.println("Upload Button clicked");
			String file="E:\\DESKTOP DEC 12th 2020\\TL BOOTCAMP BACKUP\\Assessmenttestfile.txt";
			StringSelection selection = new StringSelection(file);
			//System.out.println(selection);
			Thread.sleep(1000);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
			Thread.sleep(1000);			
			Robot rb = new Robot();
			Thread.sleep(1000);
			System.out.println("Robot created");
			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);
			Thread.sleep(1000);
			rb.keyRelease(KeyEvent.VK_CONTROL);
			rb.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			System.out.println("Attachment success");
			Thread.sleep(6000);


		} catch (Exception e) {
			System.out.println("Upload Button not Clicked");
			e.printStackTrace();
		}
	return this; 

	}

public ServiceConsolePage clickUploadDoneButton() {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//span[text()='Done']"));
		Thread.sleep(1000);
		System.out.println("UploadDone Button clicked");


	} catch (Exception e) {
		System.out.println("UploadDone Button not Clicked");
		e.printStackTrace();
	}
return this; 

}
public ServiceConsolePage clickDropDownforNewlyCreatedFile(String fileName) {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//tbody/tr/th//span[text()='"+fileName+"']//following::td[3]//div"));
		Thread.sleep(1000);
		System.out.println("Newlycreatedfile Dropdown Button clicked");


	} catch (Exception e) {
		System.out.println("Newlycreatedfile Dropdown Button not clicked");
		e.printStackTrace();
	}
return this; 

}

public ServiceConsolePage clickViewFileDetails() {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//a[@title=\"View File Details\"]"));
		Thread.sleep(1000);
		System.out.println("clickViewFileDetails Button clicked");


	} catch (Exception e) {
		System.out.println("clickViewFileDetails Button not clicked");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage verifyFileNameandExtension(String fileName,String extensionName) {
	

	boolean FileNamedisplayed=locateElement("xpath","//div[text()='File']/..//following::div[@title='"+fileName+"']").isDisplayed();
	if(FileNamedisplayed==true) {
System.out.println("Filename is displayeed and matching");
}
if(FileNamedisplayed==false) {
System.out.println("Filename is not displayeed and unsuccess");
}
//verify file extension
boolean extenDisplayed=locateElement("xpath","//span[@title=\"File Extension\"]/..//div/span[text()='"+extensionName+"']").isDisplayed();
if(extenDisplayed==true) {
System.out.println("FileExtension is displayeed and matching");
}
if(extenDisplayed==false) {
System.out.println("FileExtension is not displayeed and unsuccess");
}
return this; 
}
public ServiceConsolePage clickCloseFileWindowtab() {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//button[@title=\"Close Assessmenttestfile\"]"));
		System.out.println("CloseFilewindowTab Button clicked");


	} catch (Exception e) {
		System.out.println("CloseFilewindowTab Button not clicked");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage clickLatestModifiedItemLink(String fileName) {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//table/tbody/tr[1]/th//span[text()='"+fileName+"']"));
		System.out.println("latestModifedItem Link clicked");


	} catch (Exception e) {
		System.out.println("latestModifedItem Link not clicked");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage clickShare() {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//button/span[text()='Share']"));
		System.out.println("Share Button clicked");


	} catch (Exception e) {
		System.out.println("Share Button not clicked");
		e.printStackTrace();
	}
return this; 
}
public ServiceConsolePage clickSearchPeopleandSelectContact() {
	
	try {
		//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
		click(locateElement("xpath","//div/input[@title=\"Search People\"]"));
		Thread.sleep(1000);
		click(locateElement("xpath","//div[@title=\"Derrick Dsouza\"]"));
		System.out.println("Contact Selected");
		Thread.sleep(1000);

	} catch (Exception e) {
		System.out.println("Contact Not selected");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage verifyErrorMessageInSharePopUp() throws InterruptedException {
	
		String errorMessage = locateElement("xpath","//li[@class=\"form-element__help\"]").getText();		
		if(errorMessage.equals("Can't share file with the file owner.")) {
		System.out.println("Error message is displayed and verification success");
		}
		else {
			System.out.println("Error message is not displayed and verification unsuccess");
		
		}
		Thread.sleep(2000);

return this; 
}
public ServiceConsolePage clickCancelButtoninSharePopUp() {
	
	try {
		click(locateElement("xpath","(//button/span[text()='Cancel'])[2]"));
		System.out.println("Share Cancelled by Clicking Cancel Button");
		Thread.sleep(1000);

	} catch (Exception e) {
		System.out.println("Cancel Button not clicked");
		e.printStackTrace();
	}
return this; 
}
public ServiceConsolePage clickCloseItemDialog() {
	
	try {
		click(locateElement("xpath","//button[@title=\"Close\"]"));

		System.out.println("ItemDialog closed");


	} catch (Exception e) {
		System.out.println("ItemDialog not closed");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage clickDelete() {
	
	try {
		click(locateElement("xpath","//a[@title=\"Delete\"]"));

		System.out.println("Delete button Clicked from Dropdown");


	} catch (Exception e) {
		System.out.println("Delete button not Clicked from Dropdown");
		e.printStackTrace();
	}
return this; 
}

public ServiceConsolePage clickConfirmDelete() {
	
	try {
		click(locateElement("xpath","//span[text()='Delete']"));

		System.out.println("Confirm Delete Button clicked");


	} catch (Exception e) {
		System.out.println("Confirm Delete Button not clicked and some  exception occurs");
		e.printStackTrace();
	}
return this; 
}
public ServiceConsolePage verifyDeleteConfirmationMessage() {
	
	boolean deleteText= locateElement("Xpath","//span[@class=\"toastMessage slds-text-heading--small forceActionsText\"]").isDisplayed();
	if(deleteText==true) {
		System.out.println("Delete message is displayed and verification success");
	}
	if(deleteText==false) {
		System.out.println("Delete message is not  displayed and verification unsuccess\"");
	}


return this; 
}
}
