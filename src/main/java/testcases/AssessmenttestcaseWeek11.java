package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.AppLauncherPage;
import pages.HomePage;
import pages.LoginPage;
import pages.ServiceConsolePage;

public class AssessmenttestcaseWeek11 extends PreAndPost {
	@BeforeTest
	public void setData() {
		testCaseName = "SFO1_Week11";
		testDescription = "AssessmentTestCase";
		authors = "Saran";
		category = "smoke";
		nodes = "Service";
		dataSheetName="Assessment";
	}

@Test(dataProvider="fetchData")
  public void createTask(String username,String password,String file,String fileName, String extensionName) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName(username)
		.typePassword(password)
		.clickLogIn();
		
		new HomePage(driver, test).		
		clickAppLauncer().clickViewAll();
		
		new AppLauncherPage(driver, test)
		.clickServiceConsole();
		
		new ServiceConsolePage(driver, test)
		.clickDropDownMenu().clickFilesFromDropdown().clickUploadFiles()
		.clickUploadDoneButton().clickDropDownforNewlyCreatedFile(fileName).clickViewFileDetails().verifyFileNameandExtension(fileName, extensionName)
		.clickCloseFileWindowtab().clickLatestModifiedItemLink(fileName).clickShare()
		.clickSearchPeopleandSelectContact().verifyErrorMessageInSharePopUp().clickCancelButtoninSharePopUp().clickCloseItemDialog()
		.clickDropDownforNewlyCreatedFile(fileName).clickDelete().clickConfirmDelete()
		.verifyDeleteConfirmationMessage();
		
}
}
